#!/usr/bin/env bash

cd "$(dirname "$0")" || exit 1

ENV_FILE=./.environment
echo -e "\\e[32mCheck $ENV_FILE file...\\e[0m"
if [ ! -f $ENV_FILE ]; then
    cp ./.environment.example $ENV_FILE || exit 1
fi

echo -e "\\e[32mBuilding production images...\\e[0m"
docker-compose up -d --build || exit 1

if docker ps -f health=healthy | grep mysql; then
    echo -e "\\e[32mDatabase already setup...\\e[0m"
else
    echo -e "\\e[32mCheck mysql server status. Wait...\\e[0m"
    until docker-compose exec db /bin/bash -c 'sleep 1 && mysqladmin --silent status'; do :; done

    echo -e "\\e[32mCreate application database if necessary...\\e[0m"
    docker-compose exec db /bin/bash -c 'echo $DB_DATABASE && mysqladmin --silent create $DB_DATABASE' || echo -e "\\e[44mINFO\\e[0m application database already exists \\e[0m"
fi

echo -e "\\e[32mTurn OFF website...\\e[0m"
docker-compose exec web /bin/bash -c "php artisan down" || exit 1

echo -e "\\e[32mRun seeders and migrations...\\e[0m"

# If environment variables are not read, container needs to be restarted
docker-compose exec web /bin/bash -c "php artisan migrate --force -q" || docker-compose restart web
docker-compose exec web /bin/bash -c "php artisan db:seed --force -q" || docker-compose restart web

# If the container was restarted, migrations and seeders were not run
docker-compose exec web /bin/bash -c "php artisan migrate --force -q" || exit 1
docker-compose exec web /bin/bash -c "php artisan db:seed --force -q" || exit 1

echo -e "\\e[32mTurn ON website...\\e[0m"
docker-compose exec web /bin/bash -c "php artisan up" || exit 1

echo -e "\\e[32mSet bash colors...\\e[0m"
docker-compose exec web /bin/sh -c "echo \"export PS1='🐳 \[\033[1;36m\]web_\h \[\033[1;34m\]\W\[\033[0;35m\] \[\033[1;36m\]# \[\033[0m\]'\" >> /root/.bashrc" || exit 1
docker-compose exec db /bin/sh -c "echo \"export PS1='🐳 \[\033[1;36m\]db_\h \[\033[1;34m\]\W\[\033[0;35m\] \[\033[1;36m\]# \[\033[0m\]'\" >> /root/.bashrc" || exit 1

echo -e "\\e[32mRestarting containers...\\e[0m"
docker-compose restart web
docker-compose restart web_dev
docker-compose restart web_uat

echo -e "\\e[32mSet up cronjobs...\\e[0m"
docker-compose exec web /bin/sh -c 'echo "" > /var/www/web/storage/logs/laravel.log'
docker-compose exec web /bin/sh -c 'crontab -l > webcron'
docker-compose exec web /bin/sh -c 'echo "* * * * * cd /var/www/web && php artisan elevators:move >> /var/www/web/storage/logs/laravel.log" >> webcron' # >> /dev/null 2>&1
docker-compose exec web /bin/sh -c 'crontab webcron'
docker-compose exec web /bin/sh -c 'rm webcron'
docker-compose exec web /bin/sh -c 'service cron restart'

echo -e "\\e[32mDone!\\e[0m" || exit 1
