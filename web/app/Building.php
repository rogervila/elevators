<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'floors',
    ];

    protected $casts = [
        'floors' => 'integer',
    ];

    // Relationships
    public function elevators()
    {
        return $this->hasMany(\App\Elevator::class);
    }
}
