<?php

namespace App;

use App\Values\Floor;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Elevator extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'locked_at',
        'current_floor',
    ];

    protected $casts = [
        'current_floor' => 'integer',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'locked_at',
    ];

    // Methods
    public function lock()
    {
        $this->locked_at = now();
        $this->save();
        $this->fresh();
    }

    public function unlock()
    {
        $this->locked_at = null;
        $this->save();
        $this->fresh();
    }

    public function moveToFloor(Floor $floor)
    {
        if ($this->current_floor == $floor->getValue()) {
            \Log::warning('Elevator cannot be moved to the current floor', [
                'from' => $this->current_floor,
                'to'   => $floor->getValue(),
            ]);

            return $this;
        }

        DB::beginTransaction();

        $previousFloor = $this->current_floor;

        $this->current_floor = $floor->getValue();

        $this->save();

        $movement = Movement::create([
            'elevator_id' => $this->id,
            'from'        => $previousFloor,
            'to'          => $this->current_floor,
        ]);

        if (!$movement) {
            DB::rollback();
        }

        DB::commit();

        $this->fresh();
    }

    // Scopes
    public function scopeUnlocked($query)
    {
        return $query->whereNull('locked_at');
    }

    // Relationships
    public function building()
    {
        return $this->belongsTo(\App\Building::class);
    }

    public function movements()
    {
        return $this->hasMany(\App\Movement::class);
    }
}
