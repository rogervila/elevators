<?php

namespace App\Values;

use App\Contracts\ValueInterface;

class Floor implements ValueInterface
{
    const MINIMUM_FLOOR = 0;

    private $floor;

    public function __construct(int $floor)
    {
        $this->floor = $floor;

        $this->validate();
    }

    public function getValue()
    {
        return $this->floor;
    }

    private function validate()
    {
        if ($this->floor < self::MINIMUM_FLOOR) {
            throw new \Exception('Floor must be gte '.self::MINIMUM_FLOOR);
        }
    }
}
