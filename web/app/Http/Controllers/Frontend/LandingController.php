<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;

class LandingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __invoke()
    {
        return view('welcome');
    }
}
