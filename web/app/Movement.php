<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'elevator_id',
        'from',
        'to',
    ];

    public function calculateFloorDifference(bool $forcePositive = false): int
    {
        $result = ($this->from - $this->to) * -1;

        if ($forcePositive && $result < 0) {
            $result *= -1;
        }

        return $result;
    }

    // Relations
    public function elevator()
    {
        return $this->belongsTo(\App\Elevator::class);
    }
}
