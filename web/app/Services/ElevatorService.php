<?php

namespace App\Services;

use App\Contracts\ElevatorServiceInterface;
use App\Elevator;
use App\Petition;
use App\Values\Floor;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class ElevatorService implements ElevatorServiceInterface
{
    /**
     * Unlocked elevators ready to be moved.
     *
     * @var Collection
     */
    private $elevators;

    /**
     * Petitions to be handled.
     *
     * @var Collection
     */
    private $petitions;

    /**
     * Elevator Service process time.
     *
     * @var Carbon
     */
    private $time;

    /**
     * @param Collection $elevators
     * @param Collection $petitions
     * @param Carbon     $time
     *
     * @return void
     */
    public function process(Collection $elevators, Collection $petitions, Carbon $time)
    {
        $this->assertCollectionClass($elevators, Elevator::class);
        $this->assertCollectionClass($petitions, Petition::class);

        $this->elevators = $elevators;
        $this->petitions = $petitions;
        $this->time = $time;

        $this->handle();
    }

    private function assertCollectionClass($collection, $class)
    {
        try {
            $collection->each(function ($current) use ($class) {
                if ($class != get_class($current)) {
                    \Log::error('Collection item is not fan object from specified class', [
                        'item'          => $current,
                        'desired class' => $class,
                        'given class'   => get_class($current),
                    ]);

                    throw new \Exception($class.' should be equal to '.get_class($current));
                }
            });
        } catch (\Exception $e) {
            \Log::error('assertCollectionClass failed', [
                'exception' => $e,
            ]);

            throw new \Exception('assertCollectionClass failed');
        }
    }

    /**
     * @return void
     */
    private function handle()
    {
        $this->petitions->each(function ($petition) {
            $this->moveElevatorsFromPetition($petition);
        });
    }

    private function getFilteredChunksFromPetition(Petition $petition): Collection
    {
        $chunks = collect();

        for ($time = $petition->getHourStart()->timestamp; $time <= $petition->getHourEnd()->timestamp; $time += $petition->recursion_seconds) {
            $chunks->push(Carbon::createFromTimestamp($time));
        }

        return $chunks->filter(function ($chunk) {
            return $chunk->hour == $this->time->hour && $chunk->minute == $this->time->minute;
        });
    }

    private function moveElevatorsFromPetition(Petition $petition)
    {
        $this->getFilteredChunksFromPetition($petition)->each(function ($chunk) use ($petition) {
            $floor = new Floor($petition->floor_from);
            $elevator = $this->elevators
                ->where('building_id', $petition->building_id)
                ->where('current_floor', $floor->getValue())
                ->first();

            // NOTES: For calculation purposes, elevators move instantaneously.
            if (!is_null($elevator)) {
                $elevator->lock();
                $elevator->moveToFloor(new Floor($petition->floor_to));
                $elevator->unlock();
            }
        });
    }
}
