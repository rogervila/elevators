<?php

namespace App\Contracts;

use Carbon\Carbon;
use Illuminate\Support\Collection;

interface ElevatorServiceInterface
{
    public function process(Collection $elevators, Collection $petitions, Carbon $time);
}
