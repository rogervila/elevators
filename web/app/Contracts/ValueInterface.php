<?php

namespace App\Contracts;

interface ValueInterface
{
    public function getValue();
}
