<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Petition extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hour_start',
        'hour_end',
        'recursion_seconds',
        'floor_from',
        'floor_to',
    ];

    protected $casts = [
        'recursion_seconds' => 'integer',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    // Getters
    public function getHourStart(): Carbon
    {
        return Carbon::parse($this->hour_start);
    }

    public function getHourEnd(): Carbon
    {
        return Carbon::parse($this->hour_end);
    }
}
