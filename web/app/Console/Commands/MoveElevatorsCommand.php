<?php

namespace App\Console\Commands;

use App\Contracts\ElevatorServiceInterface;
use App\Elevator;
use App\Petition;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MoveElevatorsCommand extends Command
{
    /**
     * The ElevatorService instance.
     *
     * @var ElevatorServiceInterface
     */
    private $service;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elevators:move';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute elevator calls';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ElevatorServiceInterface $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Moving elevators...');

        try {
            $time = Carbon::now();

            $this->service->process(
                Elevator::unlocked()->get(),
                Petition::get(), // whereDatesSurround($time)->
                $time
            );

            $this->info('Success!');
        } catch (\Exception $e) {
            $this->info('Something happened');
        }
    }
}
