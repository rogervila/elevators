<?php

use App\Values\Floor;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class WayneBuildingSeeder extends Seeder
{
    const BUILDING_NAME = 'Wayne';
    const BUILDING_FLOORS = 4;
    const ELEVATORS_AMOUNT = 3;

    protected $building;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createBuilding();

        $this->createElevators();

        $this->createPetitions();
    }

    private function createPetitions()
    {
        // 1) Every 5 minutes from 09:00h to 11:00h call the elevator from the ground floor to go to the 2nd floor
        $this->makePetition(5 * 60, new Carbon('09:00h'), new Carbon('11:00h'), new Floor(0), new Floor(2));

        // 2) Every 5 minutes from 09:00h to 11:00h call the elevator from the ground floor to go to the 3rd floor
        $this->makePetition(5 * 60, new Carbon('09:00h'), new Carbon('11:00h'), new Floor(0), new Floor(3));

        // 3) Every 10 minutes from 09:00h to 10:00h call the elevator from the ground floor to the 1st floor
        $this->makePetition(10 * 60, new Carbon('09:00h'), new Carbon('10:00h'), new Floor(0), new Floor(1));

        // 4) Every 20 minutes from 11:00h to 18:20h call the elevator from the ground floor to go to all the floors
        for ($i = 0; $i < self::BUILDING_FLOORS; $i++) {
            $from = new Floor(0);
            $to = new Floor($i);

            if ($from->getValue() !== $to->getValue()) {
                $this->makePetition(20 * 60, new Carbon('11:00h'), new Carbon('18:20h'), $from, $to);
            }
        }

        // 5) Every 4 minutes from 14:00h to 15:00h call the elevator from floors 1, 2 and 3 to go to the ground floor
        foreach ([1, 2, 3] as $from) {
            $this->makePetition(4 * 60, new Carbon('14:00h'), new Carbon('15:00h'), new Floor($from), new Floor(0));
        }

        // 6) Every 7 minutes 15:00h to 16:00h call the elevator from plants 2 and 3 to go to the ground floor
        foreach ([2, 3] as $from) {
            $this->makePetition(7 * 60, new Carbon('15:00h'), new Carbon('16:00h'), new Floor($from), new Floor(0));
        }

        // 7) Every 7 minutes 15:00h to 16:00h call to lift from the ground floor to go to the floors 1 and 3
        foreach ([1, 3] as $to) {
            $this->makePetition(7 * 60, new Carbon('15:00h'), new Carbon('16:00h'), new Floor(0), new Floor($to));
        }

        // 8) Every 3 minutes from 18:00h to 20:00h call the elevator from floors 1, 2 and 3 to go to the ground floor
        foreach ([1, 2, 3] as $from) {
            $this->makePetition(3 * 60, new Carbon('18:00h'), new Carbon('20:00h'), new Floor($from), new Floor(0));
        }
    }

    private function makePetition(int $recurrency, Carbon $start, Carbon $end, Floor $from, Floor $to)
    {
        $existingPetition = DB::table('petitions')->where([
            ['building_id', '=', $this->building->id],
            ['hour_start', '=', $start->format('H:i')],
            ['hour_end', '=', $end->format('H:i')],
            ['floor_from', '=', $from->getValue()],
            ['floor_to', '=', $to->getValue()],
            ['recursion_seconds', '=', $recurrency],
        ])->first();

        if (is_null($existingPetition)) {
            DB::table('petitions')->insert([
                'building_id'       => $this->building->id,
                'hour_start'        => $start->format('H:i'),
                'hour_end'          => $end->format('H:i'),
                'floor_from'        => $from->getValue(),
                'floor_to'          => $to->getValue(),
                'recursion_seconds' => $recurrency,
                'created_at'        => now(),
                'updated_at'        => now(),
            ]);
        }
    }

    private function createElevators()
    {
        $elevators = DB::table('elevators')->where('building_id', $this->building->id)->get();

        if ($elevators->count() != self::ELEVATORS_AMOUNT) {
            DB::table('elevators')->where('building_id', $this->building->id)->delete();

            for ($i = 0; $i < self::ELEVATORS_AMOUNT; $i++) {
                DB::table('elevators')->insert([
                    'building_id' => $this->building->id,
                    'created_at'  => now(),
                    'updated_at'  => now(),
                ]);
            }
        }
    }

    private function createBuilding()
    {
        $building = DB::table('buildings')->where('name', self::BUILDING_NAME)->first();

        if (is_null($building)) {
            $building = new \stdClass();
            $building->id = DB::table('buildings')->insertGetId([
                'name'       => self::BUILDING_NAME,
                'floors'     => self::BUILDING_FLOORS,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }

        $this->building = $building;
    }
}
