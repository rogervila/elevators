<?php

use Faker\Generator as Faker;

$factory->define(App\Petition::class, function (Faker $faker) {
    return [
        'hour_start'        => $faker->time($format = 'H:i:s', $max = 'now'),
        'hour_end'          => $faker->time($format = 'H:i:s', $max = 'now'),
        'recursion_seconds' => $faker->randomElement([300, 600, 1200]),
        'floor_from'        => rand(0, 7),
        'floor_to'          => rand(0, 7),
        'building_id'       => factory(App\Building::class)->create()->id,
    ];
});
