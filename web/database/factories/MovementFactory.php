<?php

use Faker\Generator as Faker;

$factory->define(App\Movement::class, function (Faker $faker) {
    $from = rand(1, 10);
    $to = rand(1, 10);

    return [
        'elevator_id' => factory(App\Elevator::class)->create()->id,
        'from'        => $from,
        'to'          => $to,
    ];
});
