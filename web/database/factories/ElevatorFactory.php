<?php

use Faker\Generator as Faker;

$factory->define(App\Elevator::class, function (Faker $faker) {
    return [
        'building_id'   => factory(App\Building::class)->create()->id,
        'locked_at'     => null,
        'current_floor' => 0,
    ];
});
