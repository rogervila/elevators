<?php

use Faker\Generator as Faker;

$factory->define(App\Building::class, function (Faker $faker) {
    return [
        'floors' => rand(3, 10),
        'name'   => $faker->unique()->name,
    ];
});
