<?php

namespace Tests\Feature;

use App\Values\Floor;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ElevatorTest extends TestCase
{
    use DatabaseMigrations;

    public function test_elevator_can_be_locked()
    {
        $elevator = factory(\App\Elevator::class)->make();

        $this->assertTrue(is_null($elevator->locked_at));

        $elevator->lock();

        $this->assertFalse(is_null($elevator->locked_at));
    }

    public function test_elevator_can_be_unlocked()
    {
        $elevator = factory(\App\Elevator::class)->make([
            'locked_at' => now(),
        ]);

        $this->assertFalse(is_null($elevator->locked_at));

        $elevator->unlock();

        $this->assertTrue(is_null($elevator->locked_at));
    }

    public function test_elevator_can_be_moved()
    {
        $elevator = factory(\App\Elevator::class)->make([
            'current_floor' => 0,
        ]);

        $floor = new Floor(rand(1, 10));

        $elevator->moveToFloor($floor);

        $this->assertEquals($elevator->current_floor, $floor->getValue());
    }

    public function test_movement_is_created_when_elevator_is_moved()
    {
        $from = rand(0, 3);
        $to = rand(7, 10);

        $elevator = factory(\App\Elevator::class)->create([
            'current_floor' => $from,
        ]);

        $elevator->moveToFloor(new Floor($to));

        $this->assertDatabaseHas('movements', [
            'elevator_id' => $elevator->id,
            'from'        => $from,
            'to'          => $to,
        ]);
    }

    public function test_movement_fails_if_floors_are_equal()
    {
        $number = rand(100, 110);

        $elevator = factory(\App\Elevator::class)->create([
            'current_floor' => $number,
        ]);

        $elevator->moveToFloor(new Floor($number));

        $this->assertDatabaseMissing('movements', [
            'elevator_id' => $elevator->id,
            'from'        => $number,
            'to'          => $number,
        ]);
    }
}
