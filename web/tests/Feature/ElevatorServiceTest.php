<?php

namespace Tests\Feature;

use App\Elevator;
use App\Petition;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ElevatorServiceTest extends TestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        app()->bind(
            'App\Contracts\ElevatorServiceInterface',
            'App\Services\ElevatorService'
        );
    }

    public function test_fails_if_elevators_collection_does_not_contain_elevators()
    {
        $this->expectException(\Exception::class);

        app('App\Contracts\ElevatorServiceInterface')->process(
            collect(['foo', 'bar']),
            Petition::get(),
            now()
        );
    }

    public function test_fails_if_petitions_collection_does_not_contain_petitions()
    {
        $this->expectException(\Exception::class);

        app('App\Contracts\ElevatorServiceInterface')->process(
            Elevator::unlocked()->get(),
            collect(['foo', 'bar']),
            now()
        );
    }

    public function test_fail_petition()
    {
        $this->checkMissingMovements('08:59h');
        $this->checkMissingMovements('09:01h');

        $this->checkMissingMovements('09:59h');
        $this->checkMissingMovements('10:01h');

        $this->checkMissingMovements('09:04h');
        $this->checkMissingMovements('09:06h');
    }

    public function test_petition_works()
    {
        $hours = [
            '09:00h',
            '09:05h',
            '09:10h',
            '09:15h',
            '09:20h',
            '09:25h',
            '09:30h',
            '09:35h',
            '09:40h',
            '09:45h',
            '09:50h',
            '09:55h',
            '10:00h',
        ];

        foreach ($hours as $hour) {
            $this->checkFoundMovements($hour);
        }
    }

    private function checkMissingMovements(string $time)
    {
        $from = rand(0, 3);
        $to = rand(7, 10);

        $petition = factory(\App\Petition::class)->create([
            'recursion_seconds' => 300,
            'hour_start'        => '09:00',
            'hour_end'          => '10:00',
            'floor_from'        => $from,
            'floor_to'          => $to,
        ]);

        $elevator = factory(\App\Elevator::class)->create([
            'building_id'   => $petition->building_id,
            'current_floor' => $from,
        ]);

        app('App\Contracts\ElevatorServiceInterface')->process(
            Elevator::unlocked()->get(),
            Petition::get(),
            Carbon::parse($time)
        );

        $this->assertDatabaseMissing('movements', [
            'elevator_id' => $elevator->id,
            'from'        => $from,
            'to'          => $to,
        ]);
    }

    private function checkFoundMovements(string $time)
    {
        $from = rand(0, 3);
        $to = rand(7, 10);

        $petition = factory(\App\Petition::class)->create([
            'recursion_seconds' => 300,
            'hour_start'        => '09:00',
            'hour_end'          => '10:00',
            'floor_from'        => $from,
            'floor_to'          => $to,
        ]);

        $elevator = factory(\App\Elevator::class)->create([
            'building_id'   => $petition->building_id,
            'current_floor' => $from,
        ]);

        app('App\Contracts\ElevatorServiceInterface')->process(
            Elevator::unlocked()->get(),
            Petition::get(),
            Carbon::parse($time)
        );

        $this->assertDatabaseHas('movements', [
            'elevator_id' => $elevator->id,
            'from'        => $from,
            'to'          => $to,
        ]);
    }
}
