<?php

namespace Tests\Unit;

use Tests\TestCase;

class MovementTest extends TestCase
{
    public function test_movement_calculates_negative_floor_difference()
    {
        $from = 10;
        $to = 3;
        $difference = -7;

        $this->calculateFloorDifference($from, $to, $difference);
    }

    public function test_movement_calculates_positive_floor_difference()
    {
        $from = 3;
        $to = 9;
        $difference = 6;

        $this->calculateFloorDifference($from, $to, $difference);
    }

    public function test_movement_converts_difference_to_positive_by_parameter()
    {
        $from = 10;
        $to = 3;
        $difference = 7;

        $movement = new \App\Movement([
            'from' => $from,
            'to'   => $to,
        ]);

        $this->assertEquals($movement->calculateFloorDifference($forcePositive = true), $difference);
    }

    private function calculateFloorDifference(int $from, int $to, int $difference)
    {
        $movement = new \App\Movement([
            'from' => $from,
            'to'   => $to,
        ]);

        $this->assertEquals($movement->calculateFloorDifference(), $difference);
    }
}
