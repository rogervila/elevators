@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mb-5 justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Buildings</div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Name</th>
                                <th scope="col">Elevators</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($buildings as $building)
                            <tr>
                                <th scope="row">{{ $building->id }}</th>
                                <td>{{ $building->name }}</td>
                                <td>{{ $building->elevators_count }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Elevators</div>
                <div class="card-body">
                    @foreach($elevators as $elevator)
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Origin floor</th>
                                    <th scope="col">Destination floor</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($elevator->movements as $movement)
                                <tr>
                                    <th scope="row">{{ $movement->id }}</th>
                                    <td>{{ $movement->from }}</td>
                                    <td>{{ $movement->to }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
